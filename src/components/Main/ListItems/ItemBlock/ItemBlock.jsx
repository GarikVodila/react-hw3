import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import { ReactComponent as Favorite } from '../../../Header/icons/favorite.svg';
import { Button } from '../../../Button';
import { Modal } from '../../../Modal';

import './ItemBlock.scss';

function ItemBlock(props) {
    const { title, price, url, article, color, favorites, cart, addToFavorites, addToCart } = props;
    
    const [isShowModal, isSetShowModal] = React.useState(false);

    const handleModal = () => {
        if (!isShowModal) {
            document.querySelector('body').classList.add('body-no-scroll');
            isSetShowModal(isShowModal => !isShowModal);
        } else {
            document.querySelector('body').classList.remove('body-no-scroll');
            isSetShowModal(isShowModal => !isShowModal);
        }
    };

    const handleOutside = event => {
        if (!event.target.closest('.modal__content')) {
            handleModal();
        }
    };

    return (
        <>
            <div className="item__block">
                <span className="item__article">art. {article}</span>
                <h1 className="item__title">{title}</h1>
                <img className="item__img" src={url} alt="item-img" />
                <div className="item__bottom">
                    <span className="item__color">{color}</span>
                    <Favorite
                        className={cn('item__favorite', {
                            active: favorites.includes(article),
                        })}
                        onClick={() => addToFavorites(article)}
                    />
                </div>
                <div className="item__price">{price} ₴</div>
                {!cart.includes(article) && (
                    <Button className="item__btn" onClick={() => handleModal()}>
                        Add to cart
                    </Button>
                )}
                {cart.includes(article) && (
                    <Button className="item__btn" disabled>
                        Added
                    </Button>
                )}
            </div>
            {isShowModal && (
                <Modal
                    title="Add to cart"
                    handleModal={handleModal}
                    handleOutside={handleOutside}
                    addToCart={addToCart}
                    articleForCart={article}
                >
                    <div>Are you sure?</div>
                </Modal>
            )}
        </>
    );
}

ItemBlock.propTypes = {
    title: PropTypes.string,
    price: PropTypes.number,
    url: PropTypes.string,
    article: PropTypes.number,
    color: PropTypes.string,
    favorites: PropTypes.array,
    cart: PropTypes.array,
    addToFavorites: PropTypes.func,
    addToCart: PropTypes.func,
};

export default ItemBlock;
