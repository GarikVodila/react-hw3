import React from 'react';
import PropTypes from 'prop-types';

import './Cart.scss';

import { ReactComponent as Close } from '../Modal/icons/close-icon.svg';
import { Modal } from '../Modal';

function Cart({ listItems, cart, setCart }) {
    const [isShowModal, isSetShowModal] = React.useState(false);
    const [isComponentCart, setIsComponentCart] = React.useState(false);
    const [articleForCart, setArticleForCart] = React.useState();

    const deleteCart = article => {
        if (cart.includes(article)) {
            const indexArticle = cart.indexOf(article);
            cart.splice(indexArticle, 1);
            setCart([...cart]);
        }
    };

    const handleModal = () => {
        if (!isShowModal) {
            document.querySelector('body').classList.add('body-no-scroll');
            isSetShowModal(isShowModal => !isShowModal);
        } else {
            document.querySelector('body').classList.remove('body-no-scroll');
            isSetShowModal(isShowModal => !isShowModal);
        }
    };

    const handleOutside = event => {
        if (!event.target.closest('.modal__content')) {
            handleModal();
        }
    };

    return (
        <div className="cart__content">
            {cart.length > 0 && <div className="cart__title">CART</div>}
            {cart.length === 0 && <div className="cart__title cart__empty">CART is Empty</div>}
            <div className="cart__items">
                {listItems.map(
                    (item, index) =>
                        cart.includes(item.article) && (
                            <div className="cart__item" key={index}>
                                <span className="cart__item-article">{item.article}</span>
                                <h1 className="cart__item-title">{item.title}</h1>
                                <img className="cart__item_img" src={item.url} alt="item-img" />
                                <div className="cart__item-color">{item.color}</div>
                                <div className="cart__item-price">{item.price} ₴</div>
                                <div className="cart__item-icon--wrapper">
                                    <Close
                                        className="cart__item-icon"
                                        onClick={() => {
                                            setIsComponentCart(true);
                                            setArticleForCart(item.article);
                                            handleModal();
                                        }}
                                    />
                                </div>
                            </div>
                        )
                )}
            </div>
            {isShowModal && (
                <Modal
                    title="Add to cart"
                    handleModal={handleModal}
                    handleOutside={handleOutside}
                    deleteCart={deleteCart}
                    articleForCart={articleForCart}
                    isComponentCart={isComponentCart}
                >
                    <div>Are you sure?</div>
                </Modal>
            )}
        </div>
    );
}

Cart.propTypes = {
    listItems: PropTypes.array,
    cart: PropTypes.array,
    setCart: PropTypes.func,
};

export default Cart;
