import React from 'react';

import './NotPage.scss'

function NotPage() {
    return (
        <div className="notpage__content">
            <div className="notpage__info">An error occurred. There is no such page in our store.</div>
        </div>
    );
}

export default NotPage;
