import React from 'react';

import './Footer.scss';

function Footer() {
    return (
        <div className="footer__wrapper">
            <div className="container">
                <div className="footer">© Copyright 2023</div>
            </div>
        </div>
    );
}

export default Footer;
