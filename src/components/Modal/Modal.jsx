import React from 'react';
import PropTypes from 'prop-types';

import './Modal.scss';

import { Button } from '../Button';
import { ReactComponent as Close } from './icons/close-icon.svg';

function Modal({
    title,
    children,
    handleModal,
    handleOutside,
    addToCart,
    articleForCart,
    deleteCart,
    isComponentCart,
}) {
    return (
        <div className="modal" onClick={e => handleOutside(e)}>
            <div className="modal__content">
                <div className="modal__header">
                    <div className="modal__title">{title}</div>
                    <Close className="modal__close" onClick={() => handleModal()} />
                </div>
                <div className="modal__body">{children}</div>
                <div className="modal__footer">
                    {!isComponentCart && (
                        <Button
                            className="modal__footer--btn"
                            onClick={() => {
                                addToCart(articleForCart);
                                handleModal();
                            }}
                        >
                            Confirm
                        </Button>
                    )}
                    {isComponentCart && (
                        <Button
                            className="modal__footer--btn confirm"
                            onClick={() => {
                                deleteCart(articleForCart);
                                handleModal();
                            }}
                        >
                            Confirm
                        </Button>
                    )}
                    <Button className="modal__footer--btn cancel" onClick={() => handleModal()}>
                        Cancel
                    </Button>
                </div>
            </div>
        </div>
    );
}

Modal.propTypes = {
    title: PropTypes.string,
    children: PropTypes.any,
    handleModal: PropTypes.func,
    handleOutside: PropTypes.func,
    addToCart: PropTypes.func,
    articleForCart: PropTypes.number,
    deleteCart: PropTypes.func,
    isComponentCart: PropTypes.bool,
};

export default Modal;
