import React from 'react';
import PropTypes from 'prop-types';

import { ItemBlock } from './ItemBlock';

import './ListItems.scss'

function ListItems({ listItems, favorites, cart, addToFavorites, addToCart, clickConfirm, handleModal }) {
    return (
        <>
            <h1 className="list__title">ALL SHOES</h1>
            <div className="list__content">
                <div className="list__content--items">
                    {listItems &&
                        listItems.map((item, index) => (
                            <ItemBlock
                                title={item.title}
                                price={item.price}
                                url={item.url}
                                article={item.article}
                                color={item.color}
                                key={index}
                                addToFavorites={addToFavorites}
                                addToCart={addToCart}
                                favorites={favorites}
                                clickConfirm={clickConfirm}
                                handleModal={handleModal}
                                cart={cart}
                            />
                        ))}
                </div>
            </div>
        </>
    );
}

ListItems.propTypes = {
    listItems: PropTypes.array,
    favorites: PropTypes.array,
    cart: PropTypes.array,
    addToFavorites: PropTypes.func,
    addToCart: PropTypes.func,
    clickConfirm: PropTypes.func,
    handleModal: PropTypes.func,
};

export default ListItems;
